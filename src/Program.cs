﻿using System.Security.Cryptography;
using System.Text;
using Discord;
using Discord.WebSocket;

namespace DiscordR9K;

static class Program
{
    static DiscordSocketClient client;
    static readonly Robot9000 R9K = new();
    static readonly HashAlgorithm hashAlgorithm = SHA256.Create();

    public static Task Log(LogMessage msg)
    {
        Console.WriteLine(msg.ToString());
        return Task.CompletedTask;
    }

    public static void Log(LogSeverity severity, string source, string message)
    {
        Log(new(severity, source, message));
    }

    static async Task Main(string[] args)
    {
        DiscordSocketConfig clientConfig = new()
        {
            GatewayIntents = GatewayIntents.AllUnprivileged | GatewayIntents.MessageContent
        };

        client = new(clientConfig);
        client.Log += Log;

        string token = File.ReadAllText("token.txt");

        await client.LoginAsync(TokenType.Bot, token);
        await client.StartAsync();

        client.MessageReceived += OnMessageReceived;

        await Task.Delay(-1);
    }

    private static async Task OnMessageReceived(SocketMessage message)
    {
        try
        {
            if (message.Author.IsBot) return;

            StringBuilder stringBuilder = new();
            foreach (byte b in hashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(message.Content))) stringBuilder.Append(b.ToString("X2"));
            string content = stringBuilder.ToString();

            Log(LogSeverity.Verbose, "Receiver", $"Message received (hash {content})");

            if (R9K.IsWhitelisted(content) || message.Content == "")
            {
                Log(LogSeverity.Verbose, "R9K", $"Ignoring whitelisted/empty message '{message.Content}'");
            }
            else if (R9K.IsUnique(content))
            {
                R9K.Register(content);
                Log(LogSeverity.Verbose, "R9K", $"Message {content} is unique. Adding to database.");
                R9K.RefreshDatabases();
            }
            else
            {
                await message.DeleteAsync();
                await message.Author.SendMessageAsync($"Your message '{message.Content}' is a duplicate message. It is now deleted.\nMessage ID: `{content}`");
                Log(LogSeverity.Verbose, "R9K", $"Message '{message.Content}' is a duplicate of {content}");
            }
        }
        catch (Exception exception)
        {
            await message.Channel.SendMessageAsync($"Error: `{exception.Message}`");
            Log(LogSeverity.Error, "R9K", exception.ToString());
        }
    }
}
