# Discord R9K
This is an implementation of Randall Munroe's [ROBOT9000](https://blog.xkcd.com/2008/01/14/robot9000-and-xkcd-signal-attacking-noise-in-chat/) IRC Bot in Discord.