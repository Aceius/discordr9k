using Discord;
namespace DiscordR9K;

class Robot9000
{
    private string[] whitelist;
    public List<string> messages;

    public Robot9000()
    {
        Program.Log(LogSeverity.Info, "R9K", "Loading whitelist and message database");

        whitelist = File.ReadAllLines("whitelist.txt");
        messages = [.. File.ReadAllLines("messages.txt")];
    }

    public void Register(string data) => messages.Add(data);
    public bool IsUnique(string data) => !messages.Contains(data);
    public bool IsWhitelisted(string data) => whitelist.Contains(data);

    public void RefreshDatabases()
    {
        Program.Log(LogSeverity.Info, "R9K", "Refreshing whitelist and message database");

        whitelist = File.ReadAllLines("whitelist.txt");
        File.WriteAllLines("messages.txt", messages);
    }
}